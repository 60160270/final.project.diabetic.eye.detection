import cv2
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image, ImageEnhance
from skimage.filters import threshold_otsu
from scipy import ndimage
import statistics
from statistics import mode
import os

folder="../database/e-nm" # https://www.adcis.net/en/third-party/e-ophtha/

for mi in np.arange(100,101,20):
    time = 1
    for filename in os.listdir(folder):
        # Brightness enchancement
        For = Image.open(os.path.join(folder,filename))
        # For = Image.open('../preprocessing/C0001883.jpg')
        # For = Image.open('../preprocessing/ddb1/image0'+str(time)+'.png')
        For = For.resize((1500, 1152))
        enhancer = ImageEnhance.Brightness(For)
        factor = 1.0  # 2.25
        Fbc = enhancer.enhance(factor)
        Fbc.save('../preprocessing/result_3/brightness.png')

        # extract green channel
        Fbc = cv2.imread('../preprocessing/result_3/brightness.png')
        Fg = Fbc[:, :, 1]
        cv2.imwrite('../preprocessing/result_3/extractg.png', Fg)

        # Start of CLAHE
        Fg = cv2.imread('../preprocessing/result_3/extractg.png')
        Fg = cv2.cvtColor(Fg, cv2.COLOR_BGR2GRAY)
        clahe = cv2.createCLAHE(clipLimit=4)
        Fcl = clahe.apply(Fg)
        cv2.imwrite('../preprocessing/result_3/clahe.png', Fcl)
        # End of CLAHE

        #  Median filter 80*80
        Fcl = cv2.imread('../preprocessing/result_3/clahe.png')
        Fcl = cv2.cvtColor(Fcl, cv2.COLOR_BGR2GRAY)
        cv2.imwrite('../preprocessing/result_3/Fmi.png',
                    ndimage.median_filter(Fcl, 80))
        # Median filter

        # Substracted Fcl Fmi
        Fcl = cv2.imread('../preprocessing/result_3/clahe.png')
        Fmi = cv2.imread('../preprocessing/result_3/Fmi.png')
        GFcl = cv2.cvtColor(Fcl, cv2.COLOR_BGR2GRAY)
        GFmi = cv2.cvtColor(Fmi, cv2.COLOR_BGR2GRAY)
        Fpm = cv2.subtract(GFmi, GFcl)
        cv2.imwrite('../preprocessing/result_3/Fpm.png', Fpm)
        # Substracted Fcl Fmi

        # Median filter three times
        Fpm = cv2.imread('../preprocessing/result_3/Fpm.png')
        GFpm = cv2.cvtColor(Fpm, cv2.COLOR_BGR2GRAY)
        Fpm = ndimage.median_filter(GFpm, 3)
        Fpm = ndimage.median_filter(Fpm, 3)
        Fbm = ndimage.median_filter(Fpm, 3)
        # Median filter three times

        SD = np.std(Fbm)  # standard deviation of bm
        print(SD)
        print(np.var(Fbm))
        # Eq 2 Fbm > T = Fbm ; else 0
        Fse = Fbm.copy()  # Less than zero Change to zero
        Fse[Fse <= SD] = 0

        plt.imsave('../preprocessing/result_3/Fse.png', Fse, cmap='gray')
        plt.imsave('../preprocessing/result_3/Fwh.png', Fse, cmap='gray')

        # Start of K MEAN using K = 2
        img = cv2.imread('../preprocessing/result_3/Fwh.png')
        vectorized = img.reshape((-1, 3))
        vectorized = np.float32(vectorized)
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        attempts = 10
        K = 2
        ret, label, center = cv2.kmeans(vectorized, K, None, criteria, attempts, cv2.KMEANS_PP_CENTERS)
        center = np.uint8(center)
        res = center[label.flatten()]
        result_image = res.reshape((img.shape))
        cv2.imwrite('../preprocessing/result_3/kmean.png', result_image)
        # K MEAN

        # ============================================================
        Fk = cv2.imread('../preprocessing/result_3/kmean.png')
        Fk = cv2.cvtColor(Fk, cv2.COLOR_BGR2GRAY)

        # Otsu threshold
        cleaningImg = cv2.imread('../preprocessing/result_3/kmean.png')
        cleaningImg = cv2.cvtColor(cleaningImg, cv2.COLOR_BGR2GRAY)
        val = threshold_otsu(cleaningImg)
        # plt.imsave('../preprocessing/result_3/otsu.png',cleaningImg > val,cmap='gray')
        plt.imsave('../preprocessing/result_3/otsu.png', cleaningImg > val, cmap='gray')
        # Otsu threshold

        # optic disk detection
        # Fcl change to extractg
        Fcl = cv2.imread('../preprocessing/result_3/Fmi.png', 0)
        for thresh_val in np.arange(255, 0, -5):
            Fcl_copy = Fcl.copy()
            Fcl_copy[Fcl_copy < thresh_val] = 0
            _, optic_img = cv2.connectedComponents(Fcl_copy)
            if optic_img[optic_img > 0].flatten().size > 0 :
                optic_img[optic_img != mode(optic_img[optic_img > 0].flatten())] = 0
                plt.imsave('../preprocessing/result_3/opticdisk_detect.png',optic_img, cmap='gray')
                break

            # Fcl[Fcl<255] = 0
            # _, optic_img = cv2.connectedComponents(Fcl)
            # optic_img[optic_img!=mode(optic_img[optic_img>0].flatten())] = 0

            # plt.imsave('../preprocessing/result_3/opticdisk_detect.png',optic_img,cmap='gray')

        optic_img = cv2.imread('../preprocessing/result_3/opticdisk_detect.png', 0)
        contours = cv2.findContours(optic_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]

        box = []
        for cnt in contours:
            rect = cv2.minAreaRect(cnt)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            # img = cv2.drawContours(img,[box],0,(0,255,0),3)
        x = 0
        y = 0
        for point in box:
            x = x+point[0]
            y = y+point[1]
        x = int(x/len(box))
        y = int(y/len(box))
        # if(x < 750):
        #     # x = x-35
        #     x = x-35
        # else:
        #     # x = x+35
        #     x = x+35
        x = int(x)
        img = np.zeros(optic_img.shape)
        optic_img = cv2.circle(img, (x, y), radius=100,color=(255, 0, 0), thickness=-1)

        cv2.imwrite('../preprocessing/result_3/optic.png', img)
        # optic disk detection

        # Subtract
        thre_img = cv2.imread('../preprocessing/result_3/optic.png')
        optic_img = cv2.imread('../preprocessing/result_3/otsu.png')
        thre_img = cv2.cvtColor(thre_img, cv2.COLOR_BGR2GRAY)
        optic_img = cv2.cvtColor(optic_img, cv2.COLOR_BGR2GRAY)
        test = cv2.add(thre_img, optic_img)
        # test = cv2.subtract(thre_img,optic_img)
        plt.imsave('../preprocessing/result_3/optic_otsu'+str(time)+'.png', test, cmap='gray')
        # Subtract

        # connect component
        img = cv2.imread('../preprocessing/result_3/optic_otsu'+str(time)+'.png', 0)
        num_labels, labels_im = cv2.connectedComponents(img)

        def imshow_components(labels):
            # Map component labels to hue val
            label_hue = np.uint8(179*labels/np.max(labels))
            blank_ch = 255*np.ones_like(label_hue)
            labeled_img = cv2.merge([label_hue, blank_ch, blank_ch])

            # cvt to BGR for display
            labeled_img = cv2.cvtColor(labeled_img, cv2.COLOR_HSV2BGR)

            # set bg label to black
            labeled_img[label_hue == 0] = 0
            # cv2.imshow('labeled.png', labeled_img)

            # Morphological Transformations
            dilation = cv2.dilate(labeled_img, np.ones((2, 2), np.uint8), iterations=1)
            opening = cv2.morphologyEx(dilation, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
            closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
            median = cv2.medianBlur(closing, 5)
            # Morphological Transformations
            cv2.imwrite('../preprocessing/result_3/hemorrhage.png', median)
            # cv2.waitKey()

        labels_im[labels_im == mode(labels_im[labels_im > 0].flatten())] = 0
        # labels_im[labels_im == mode(labels_im[labels_im > 0].flatten())] = 0
        imshow_components(labels_im)
        # connect component

        def most_frequent(List): 
            return max(set(List), key = List.count) 

        # binary hemorrhage
        hem_img = cv2.imread('../preprocessing/result_3/hemorrhage.png', 0)
        # _, img = cv2.connectedComponents(img)
        most_num = most_frequent(hem_img[hem_img>0].flatten().tolist())
        while hem_img[hem_img==most_num].size > 1000:
            hem_img[hem_img==most_num] = 0
            most_num = most_frequent(hem_img[hem_img>0].flatten().tolist())
        hem_img[hem_img > 0] = 1
        plt.imsave('../preprocessing/'+str(time)+'.png', hem_img, cmap='gray')
        # binary hemorrhage

        time = time +1
        break