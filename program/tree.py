from sklearn import tree
import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split
from sklearn import metrics
import statistics
from statistics import mode

from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier

array = []
filea = 100
string_file = str(filea)+'x'+str(filea)
for time in np.arange(1, 20, 1):
    array_nm = []
    img = cv2.imread('../preprocessing/'+string_file+'/dr'+str(time)+'.png', 0)
    nm_len = len(img[img > 0].flatten())
    num_labels, labels_im = cv2.connectedComponents(img)
    array_nm.append(np.mean(img))
    array_nm.append(np.std(img))
    array_nm.append(nm_len)
    array_nm.append(num_labels)
    array_nm.append(1)
    array.append(array_nm)
for time in np.arange(1, 144, 1):
    array_nm = []
    img = cv2.imread('../preprocessing/'+string_file+'/nm'+str(time)+'.png', 0)
    nm_len = len(img[img > 0].flatten())
    num_labels, labels_im = cv2.connectedComponents(img)
    array_nm.append(np.mean(img))
    array_nm.append(np.std(img))
    array_nm.append(nm_len)
    array_nm.append(num_labels)
    array_nm.append(0)
    array.append(array_nm)

X = np.array(array)[:, 0:4]
y = np.array(array)[:, 4]

# Decision tree
result = []
for i in np.arange(1,11,1):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(X_train, y_train)
    y_pred = clf .predict(X_test)
    value = metrics.accuracy_score(y_test, y_pred)*100
    result.append(value)

print('Decision tree :')
print(result)
print('mean : '+str(np.mean(result)))
# Devision tree

# KNN
for n in np.arange(1,10,2):
    result = []
    for i in np.arange(1,11,1):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)
        classifier = KNeighborsClassifier(n_neighbors=n)
        classifier.fit(X_train, y_train)
        y_pred = classifier.predict(X_test)
        value = metrics.accuracy_score(y_test, y_pred)*100
        result.append(value)
    print('KNN '+str(n)+' :')
    print(result)
    print('mean : '+str(np.mean(result)))
# KNN

# Random forest
for n in np.arange(11,18,2):
    result = []
    for i in np.arange(1,11,1):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)
        rfor = RandomForestClassifier(n_estimators=n)
        rfor.fit(X,y)
        y_pred = rfor.predict(X_test)
        value = metrics.accuracy_score(y_test, y_pred)*100
        result.append(value)

    print('Random forest '+str(n)+' :')
    print(result)
    print('mean : '+str(np.mean(result)))
# Random forest